package com.endava.homework.bonustask.firsttask;

/**
 * Refactor worstMethodEver to be more readable, but do not change method
 */
public class WorstClassEverRefactored {

    private static final int BABY_ELEPHANT_MIN_WEIGHT = 50;
    private static final int BABY_ELEPHANT_MAX_WEIGHT = 90;
    private static final int OLD_ELEPHANT_MIN_WEIGHT = 100;
    private static final int OLD_ELEPHANT_MAX_WEIGHT = 300;


    public String worstMethodEver(boolean isPressed, int elephantsAge, double elephantsWeight) {
        if (isPressed) {
            return getResolution(elephantsAge, elephantsWeight);
        }
        return "please press the button";
    }

    private String getResolution(int elephantsAge, double elephantsWeight) {
        if (isOldElephant(elephantsAge)) {
            return getResolutionForOldElephant(elephantsWeight);
        } else {
            return getResolutionForBabyElephant(elephantsWeight);
        }
    }

    boolean isOldElephant(int elephantsAge) {
        return (elephantsAge >= 2);
    }

    String getResolutionForBabyElephant(double elephantsWeight) {
        if (elephantsWeight > BABY_ELEPHANT_MAX_WEIGHT) {
            return "This baby elephant should eat less";
        } else if (elephantsWeight < BABY_ELEPHANT_MIN_WEIGHT) {
            return "This baby elephant should eat more";
        }
        return "This baby elephant is healthy";
    }

    String getResolutionForOldElephant(double elephantsWeight) {
        if (elephantsWeight >  OLD_ELEPHANT_MAX_WEIGHT) {
            return "This elephant should eat less";
        } else if (elephantsWeight < OLD_ELEPHANT_MIN_WEIGHT) {
            return "This elephant should eat more";
        }
        return "This elephant is healthy";

    }
}
