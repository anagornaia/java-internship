package com.endava.homework.bonustask.firsttask;

public class WorstClassEver {

    public String worstMethodEver(boolean isPressed, int elephantsAge, double elephantsWeight) {

        if (isPressed) {
            if (elephantsAge >= 2) {
                if (elephantsWeight >= 100 && elephantsWeight <= 300) {
                    return "This elephant is healthy";
                } else if (elephantsWeight < 100) {
                    return "This elephant should eat more";
                } else if (elephantsWeight > 300) {
                    return "This elephant should eat less";
                }
            } else if (elephantsAge < 2) {
                if (elephantsWeight >= 50 && elephantsWeight <= 90) {
                    return "This baby elephant is healthy";
                } else if (elephantsWeight < 50) {
                    return "This baby elephant should eat more";
                } else if (elephantsWeight > 90) {
                    return "This baby elephant should eat less";
                }
            }
        } else if (!isPressed){
            return "please press the button";
        }

        return "ha, gotcha";
    }
}

