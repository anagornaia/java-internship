package com.endava.homework;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StudentTest {
    @Test
    public void shouldSortByNameAndAge() {
        List<Student> students = new ArrayList<>();
        students.add(new Student("Anton", LocalDate.now().minusYears(24), "details 1"));
        students.add(new Student("Ivan", LocalDate.now().minusYears(24), "details 1"));
        students.add(new Student("Anton", LocalDate.now().minusYears(25), "details 1"));

        students.sort(Student::compareTo);

        assertEquals(buildExpectedData(), students);
    }

    List<Student> buildExpectedData() {
        List<Student> students = new ArrayList<>();
        students.add(new Student("Anton", LocalDate.now().minusYears(25), "details 1"));
        students.add(new Student("Anton", LocalDate.now().minusYears(24), "details 1"));
        students.add(new Student("Ivan", LocalDate.now().minusYears(24), "details 1"));
        return students;
    }
}
