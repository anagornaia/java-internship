package com.endava.homework.bonustask.firsttask;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;


class WorstClassEverTest {

    private WorstClassEver worstClassEver = new WorstClassEver();
    private WorstClassEverRefactored worstClassEverRefactored = new WorstClassEverRefactored();

    /**
     * Cover worstMethodEver as much as possible
     * Test coverage:
     * Class = 100%
     * Method = 100%
     * Line = 94% -> line 29 can not be covered @see WorstClassEver
     */
    @ParameterizedTest
    @MethodSource("testData")
    void worstMethodEver(boolean isPressed, int elephantsAge, double elephantsWeight, String expected) {
        assertThat(worstClassEver.worstMethodEver(isPressed, elephantsAge, elephantsWeight)).isEqualTo(expected);
    }

    /**
     * Cover refactored worstMethodEver as much as possible
     * Test coverage:
     * Class = 100%
     * Method = 100%
     * Line = 100%
     */
    @ParameterizedTest
    @MethodSource("testData")
    void worstMethodEverRefactored(boolean isPressed, int elephantsAge, double elephantsWeight, String expected) {
        assertThat(worstClassEverRefactored.worstMethodEver(isPressed, elephantsAge, elephantsWeight)).isEqualTo(expected);
    }

    private static Stream<Arguments> testData() {
        return Stream.of(
            Arguments.of(true, 2, 100, "This elephant is healthy"),
            Arguments.of(true, 2, 150, "This elephant is healthy"),
            Arguments.of(true, 2, 50, "This elephant should eat more"),
            Arguments.of(true, 2, 301, "This elephant should eat less"),
            Arguments.of(true, 1, 50, "This baby elephant is healthy"),
            Arguments.of(true, 1, 60, "This baby elephant is healthy"),
            Arguments.of(true, 1, 49, "This baby elephant should eat more"),
            Arguments.of(true, 1, 91, "This baby elephant should eat less"),
            Arguments.of(false, 1, 91, "please press the button")
        );
    }
}