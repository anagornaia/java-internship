package com.endava.homework;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class StudentTreeMapTest {

    private StudentTreeMap studentTreeMap;

    private static final Student KEY = new Student("Anton", LocalDate.now().minusYears(25), "details 1");
    private static final Integer VALUE = 4;

    @BeforeEach
    public void setUp() {
        studentTreeMap = setUpStudentTreeMap();
    }

    @Test
    public void size() {
        assertThat(studentTreeMap).hasSize(3);
    }

    @Test
    public void isEmpty() {
        assertThat(studentTreeMap).isNotEmpty();
    }

    @ParameterizedTest
    @MethodSource("provideKeys")
    public void containsKey(Student key) {
        assertThat(studentTreeMap).containsKey(key);
    }

    @Test
    public void containsValue() {
        assertThat(studentTreeMap).containsValue(VALUE);
    }

    @Test
    public void containsKeyExpectedException() {
        assertThatThrownBy(
            () -> studentTreeMap.containsKey("some key")
        ).isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    public void containsValueExpectedException() {
        assertThatThrownBy(
            () -> studentTreeMap.containsValue("some value")
        ).isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    public void get() {
        assertThat(studentTreeMap.get(KEY)).isEqualTo(VALUE);
    }

    @ParameterizedTest
    @MethodSource("provideKeys")
    public void remove(Student key) {
        studentTreeMap.remove(key);
        assertThat(studentTreeMap).doesNotContainKey(key);
    }

    @Test
    public void putAll() {
        studentTreeMap.putAll(setUpMoreStudentTreeMap());
        setUpMoreStudentTreeMap().forEach((key, value) -> assertThat(studentTreeMap).containsEntry(key, value));
    }

    @Test
    public void clear() {
        studentTreeMap.clear();
        assertThat(studentTreeMap).isEmpty();
    }

    @Test
    public void keySet() {
        assertThat(studentTreeMap.keySet()).contains(expectedKeySet().toArray(new Student[0]));
    }

    @Test
    public void values() {
        assertThat(studentTreeMap.values()).containsExactlyInAnyOrder(2, 3, 4);
    }

    @Test
    public void entrySet() {
        assertThat(studentTreeMap.entrySet()).containsExactlyInAnyOrder(expectedEntryKeySet().toArray(new StudentTreeMap.Node[0]));
    }

    private StudentTreeMap setUpStudentTreeMap() {
        return new StudentTreeMap() {{
            put(new Student("Anton", LocalDate.now().minusYears(25), "details 1"), 1);
            put(new Student("Ivan", LocalDate.now().minusYears(24), "details 2"), 2);
            put(new Student("Alex", LocalDate.now(), "details 3"), 3);
            put(new Student("Anton", LocalDate.now().minusYears(25), "details 1"), 4);
        }};
    }

    private HashMap<Student, Integer> setUpMoreStudentTreeMap() {
        return new HashMap<Student, Integer>() {{
            put(new Student("Chen", LocalDate.now().minusYears(26), "details 4"), 1);
            put(new Student("Li", LocalDate.now().minusYears(27), "details 5"), 2);
            put(new Student("Van", LocalDate.now(), "details 6"), 3);
            put(new Student("Dan", LocalDate.now().minusYears(89), "details 7"), 4);
        }};
    }

    HashSet<Student> expectedKeySet() {
        return new HashSet<Student>() {{
            add(new Student("Anton", LocalDate.now().minusYears(25), "details 1"));
            add(new Student("Ivan", LocalDate.now().minusYears(24), "details 2"));
            add(new Student("Alex", LocalDate.now(), "details 3"));
        }};
    }

    HashSet<Map.Entry<Student, Integer>> expectedEntryKeySet() {
        return new HashSet<Map.Entry<Student, Integer>>() {{
            add(new StudentTreeMap.Node(new Student("Anton", LocalDate.now().minusYears(25), "details 1"), 4));
            add(new StudentTreeMap.Node(new Student("Ivan", LocalDate.now().minusYears(24), "details 2"), 2));
            add(new StudentTreeMap.Node(new Student("Alex", LocalDate.now(), "details 3"), 3));

        }};
    }

    private static Stream<Arguments> provideKeys() {
        return Stream.of(
            Arguments.of(new Student("Anton", LocalDate.now().minusYears(25), "details 1")),
            Arguments.of(new Student("Ivan", LocalDate.now().minusYears(24), "details 2")),
            Arguments.of(new Student("Alex", LocalDate.now(), "details 3"))
        );
    }
}