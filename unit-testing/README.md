# Homework 2. Unit testing

##The Objective 

Learn to write unit tests.  

##The assignment 

For this homework you will be writing unit tests for your previous homework: the collections.  

The time has come to move your project to maven now. If you haven’t done so before (which was not mandatory with collections homework). It’s time to do it now.  

Given the fact that moving an already existing project to maven is kind of difficult and painful, I recommend you the following steps: 

- Start a new project from scratch. Create a new repository for it 

- In Intelij create a new maven project, use archetype quickstart 

- Create your empty initial commit. (you are in master branch) 

- Copy your collections in the new maven project structures. 

- Commit your collections to master branch 

- Create a new branch for your unit tests 

- Write your unit tests, test them 

- Commit and PR. 