package com.endava.homework;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @see java.util.TreeMap
 */
public class StudentTreeMap implements Map<Student, Integer> {

    public static class Node implements Map.Entry<Student, Integer> {
        Student key;
        Integer value;
        Node leftChild;
        Node rightChild;

        public Node(Student key, Integer value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public Student getKey() {
            return key;
        }

        @Override
        public Integer getValue() {
            return value;
        }

        @Override
        public Integer setValue(Integer value) {
            Integer oldValue = this.value;
            this.value = value;
            return oldValue;
        }

        public boolean equals(Object o) {
            if (!(o instanceof Map.Entry))
                return false;
            Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;

            return key.equals(e.getKey()) && value.equals(e.getValue());
        }

        public int hashCode() {
            int keyHash = (key == null ? 31 : key.hashCode());
            int valueHash = (value == null ? 31 : value.hashCode());
            return keyHash ^ valueHash;
        }

        @Override
        public String toString() {
            return "Node{" +
                "key=" + key +
                ", value=" + value +
                '}';
        }
    }

    private Node root;
    private Set<Student> keySet = new HashSet<>();
    private Collection<Integer> values = new ArrayList<>();
    private Set<Entry<Student, Integer>> entrySet = new HashSet<>();

    @Override
    public int size() {
        if (entrySet() != null)
            return entrySet.size();
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return (size() == 0);
    }

    @Override
    public boolean containsKey(Object o) {
        return get(o) != null;
    }

    @Override
    public boolean containsValue(Object o) {
        if (o instanceof Integer)
            return values().contains(Integer.valueOf(o.toString()));
        throw new UnsupportedOperationException("Value should be " + Integer.class);
    }

    @Override
    public Integer get(Object o) {
        if (o instanceof Student) {
            return get((Student) o);
        }
        throw new UnsupportedOperationException("Key type should be " + Student.class);
    }

    public Integer get(Student key) {
        Node node = getNode(key);
        if (node != null) {
            return node.getValue();
        }
        return null;
    }

    private Node getNode(Student key) {
        Node current = root;
        while (!current.getKey().equals(key)) {
            if (key.compareTo(current.getKey()) < 0) {//go left
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }

            if (current == null) {
                return null;
            }
        }
        return current;
    }

    @Override
    public Integer put(Student key, Integer value) {
        Node node = new Node(key, value);
        if (root == null) {
            root = node;
        } else {
            Node current = root;
            Node parent;
            while (true) {
                parent = current;
                if (current.key.equals(node.key)) {
                    return current.setValue(node.value);
                }
                if (node.getKey().compareTo(current.getKey()) < 0) {//go left
                    current = current.leftChild;
                    if (current == null) {
                        parent.leftChild = node;
                        return value;
                    }
                } else {//go right
                    current = current.rightChild;
                    if (current == null) {
                        parent.rightChild = node;
                        return value;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public Integer remove(Object o) {
        if (size() != 0) {
            Node parent = root;
            Node current = root;
            boolean isLeftChild = true;

            Student key = (Student) o;
            Node node = getNode(key);
            while (!current.getKey().equals(key)) {
                parent = current;
                if (key.compareTo(current.getKey()) < 0) {
                    isLeftChild = true;
                    current = current.leftChild;
                } else {
                    isLeftChild = false;
                    current = current.rightChild;
                }
                if (current == null)
                    return null;
            }
//one child
            if (current.leftChild == null && current.rightChild == null) {
                if (current.equals(root)) {
                    root = null;
                } else if (isLeftChild) {
                    parent.leftChild = null;
                } else {
                    parent.rightChild = null;
                }
            } else if (current.rightChild == null) {
                if (current == root) {
                    root = current.leftChild;
                } else if (isLeftChild) {
                    parent.leftChild = current.leftChild;
                } else {
                    parent.rightChild = current.leftChild;
                }
            } else if (current.leftChild == null) {
                if (current == root) {
                    root = current.rightChild;
                } else if (isLeftChild) {
                    parent.leftChild = current.rightChild;
                } else {
                    parent.rightChild = current.rightChild;
                }
            } else {//two children
                Node successor = getSuccessor(current);

                if (current.equals(root)) {
                    root = successor;
                } else if (isLeftChild) {
                    parent.leftChild = successor;
                } else {
                    parent.rightChild = successor;
                }
                successor.leftChild = current.leftChild;
            }
            return node.getValue();
        }
        return null;
    }

    private Node getSuccessor(Node node) {
        Node successorParent = node;
        Node successor = node;
        Node current = node.rightChild;

        while (current != null) {
            successorParent = successor;
            successor = current;
            current = current.leftChild;
        }

        if (successor != node.rightChild) {
            successorParent.leftChild = successor.rightChild;
            successor.rightChild = node.rightChild;
        }
        return successor;
    }

    @Override
    public void putAll(Map<? extends Student, ? extends Integer> map) {
        for (Map.Entry<? extends Student, ? extends Integer> entry : map.entrySet()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        values = null;
        keySet = null;
        entrySet = null;
        root = null;
    }

    @Override
    public Set<Student> keySet() {
        traverse(root);
        return entrySet.stream().map(Entry::getKey).collect(Collectors.toSet());
    }

    @Override
    public Collection<Integer> values() {
        traverse(root);
        return entrySet.stream().map(Entry::getValue).collect(Collectors.toList());
    }

    private void traverse(Node localRoot) {
        if (localRoot != null) {
            traverse(localRoot.leftChild);
            entrySet.add(new Node(localRoot.key, localRoot.value));
            traverse(localRoot.rightChild);
        }
    }

    @Override
    public Set<Entry<Student, Integer>> entrySet() {
        traverse(root);
        return entrySet;
    }
}
