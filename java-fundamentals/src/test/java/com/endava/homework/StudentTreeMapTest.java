package com.endava.homework;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class StudentTreeMapTest {
    private StudentTreeMap studentTreeMap;

    private static final Student KEY = new Student("Anton", LocalDate.now().minusYears(25), "details 1");
    private static final Student NEW_KEY = new Student("Chen", LocalDate.now().minusYears(26), "details 4");
    private static final Integer VALUE = 4;

    @BeforeEach
    public void setUp() {
        studentTreeMap = setUpStudentTreeMap();
    }

    @Test
    public void size() {
        assertEquals(3, studentTreeMap.size());
    }

    @Test
    public void isEmpty() {
        assertFalse(studentTreeMap.isEmpty());
    }

    @Test
    public void containsKey() {
        assertTrue(studentTreeMap.containsKey(KEY));
    }

    @Test
    public void containsValue() {
        assertTrue(studentTreeMap.containsValue(VALUE));
    }

    @Test
    public void get() {
        assertEquals(VALUE, studentTreeMap.get(KEY));
    }

    @Test
    public void remove() {
        studentTreeMap.remove(KEY);
        assertFalse(studentTreeMap.containsKey(KEY));
    }

    @Test
    public void putAll() {
        studentTreeMap.putAll(setUpMoreStudentTreeMap());
        assertTrue(studentTreeMap.containsKey(NEW_KEY));
    }

    @Test
    public void clear() {
        studentTreeMap.clear();
        assertTrue(studentTreeMap.isEmpty());
    }

    @Test
    public void keySet() {
        assertEquals(expectedKeySet(), studentTreeMap.keySet());
    }

    @Test
    public void values() {
        assertThat(studentTreeMap.values()).containsExactlyInAnyOrder(2, 3, 4);
    }

    @Test
    public void entrySet() {
        assertEquals(expectedEntryKeySet(), studentTreeMap.entrySet());
    }

    private StudentTreeMap setUpStudentTreeMap() {
        return new StudentTreeMap() {{
            put(new Student("Anton", LocalDate.now().minusYears(25), "details 1"), 1);
            put(new Student("Ivan", LocalDate.now().minusYears(24), "details 2"), 2);
            put(new Student("Alex", LocalDate.now(), "details 3"), 3);
            put(new Student("Anton", LocalDate.now().minusYears(25), "details 1"), 4);
        }};
    }

    private HashMap<Student, Integer> setUpMoreStudentTreeMap() {
        return new HashMap<Student, Integer>() {{
            put(new Student("Chen", LocalDate.now().minusYears(26), "details 4"), 1);
            put(new Student("Li", LocalDate.now().minusYears(27), "details 5"), 2);
            put(new Student("Van", LocalDate.now(), "details 6"), 3);
            put(new Student("Dan", LocalDate.now().minusYears(89), "details 7"), 4);
        }};
    }

    HashSet<Student> expectedKeySet() {
        return new HashSet<Student>() {{
            add(new Student("Anton", LocalDate.now().minusYears(25), "details 1"));
            add(new Student("Ivan", LocalDate.now().minusYears(24), "details 2"));
            add(new Student("Alex", LocalDate.now(), "details 3"));
        }};
    }

    HashSet<Map.Entry<Student, Integer>> expectedEntryKeySet() {
        return new HashSet<Map.Entry<Student, Integer>>() {{
            add(new StudentTreeMap.Node(new Student("Anton", LocalDate.now().minusYears(25), "details 1"), 4));
            add(new StudentTreeMap.Node(new Student("Ivan", LocalDate.now().minusYears(24), "details 2"), 2));
            add(new StudentTreeMap.Node(new Student("Alex", LocalDate.now(), "details 3"), 3));

        }};
    }
}