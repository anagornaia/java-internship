# Homework 1. TreeMap implementation

You will create a collection that implements a specific interface. Your collection needs to internally function in a manner similar to the well-known implementations. For example: HashMap and ArrayList will internally have an array to store data. The data stored in your collection will be a Student object. Maps will store Student as key, and an arbitrary Object as a value (your choice, can be an Integer with value of Student’s age).

Your collection will store objects of Student class inside. You might need to make some adjustments for this class to properly work with your collection. Consider making the necessary adjustments.

- Student class
```java
public class Student //TODO consider implementing any interfaces necessary for your collection
{
    private String name;
    private LocalDate dateOfBirth;
    private String details;

    public Student(String name, LocalDate dateOfBirth, String details) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.details = details;
    }

    public String getName() { return name; }

    public LocalDate getDateOfBirth() { return dateOfBirth; }

    public String getDetails() { return details; }

    /*TODO consider overriding any methods for this object to function properly within a collection:
        1. A student is considered unique by a combination of their name and dateOfBirth
        2. Student names are sorted alphabetically, if two students have the same name, then the older one is
        placed before the younger student in an ordered student list.
    */
}
```

- Map 

The Student class will act as Key for your map. You can store anything you like as a Value.

**TreeMap** internals are similar to TreeSet internals. Most important thing about your TreeMap implementation: IT’S OK FOR YOUR TREES IMPLEMENTATION TO BE UNBALANCED. Unbalanced means that the tree will not rebalance itself with every object insertion (the RED-BLACK trees do that). This means that if you insert items in sorted order, the tree will not be efficient at searching (it will essentially be a LinkedList). 
Self-Balancing Trees (like Red-Black) are out of scope fort this homework. 
```java
public class StudentMap implements Map<Student, Integer> {
    @Override
    public int size() {
        //TODO
        return 0;
    }

    @Override
    public boolean isEmpty() {
        //TODO
        return false;
    }

    @Override
    public boolean containsKey(Object o) {
        //TODO
        return false;
    }

    @Override
    public boolean containsValue(Object o) {
        //TODO
        return false;
    }

    @Override
    public Integer get(Object o) {
        //TODO
        return null;
    }

    @Override
    public Integer put(Student student, Integer integer) {
        //TODO
        return null;
    }

    @Override
    public Integer remove(Object o) {
        //TODO
        return null;
    }

    @Override
    public void putAll(Map<? extends Student, ? extends Integer> map) {
        //TODO
    }

    @Override
    public void clear() {
        //TODO
    }

    @Override
    public Set<Student> keySet() {
        //TODO
        return null;
    }

    @Override
    public Collection<Integer> values() {
        //TODO
        return null;
    }

    @Override
    public Set<Entry<Student, Integer>> entrySet() {
        //Ignore this for homework
        throw new UnsupportedOperationException();
    }
}
```