-- write a query to display:
-- 1. the first name, last name, department number, and department name for each employee.
select first_name, last_name, department_id, department_name
from employees e left join departments d using(department_id);

-- 2. the first and last name, department, city, and state province for each employee.
select hr.employees.first_name, hr.employees.last_name, hr.departments.department_name, hr.locations.city, hr.locations.state_province
from  hr.employees
left join hr.departments on hr.employees.department_id = hr.departments.department_id
left join hr.locations on hr.locations.location_id = hr.departments.location_id

-- 3. the first name, last name, salary, and job grade for all employees.
select hr.employees.first_name, hr.employees.last_name, hr.employees.salary, hr.jobs.job_title
from  hr.employees
left join hr.jobs on hr.employees.job_id = hr.jobs.job_id

-- 4. the first name, last name, department number and department name, for all employees for departments 80 or 40.
select hr.employees.first_name, hr.employees.last_name, hr.departments.department_id, hr.departments.department_name
from  hr.employees
join hr.departments on hr.employees.department_id = hr.departments.department_id
where hr.employees.department_id in (40, 80)

-- 5. those employees who contain a letter z to their first name and also display their last name, department, city, and state province.
select hr.employees.last_name, hr.departments.department_name, hr.locations.city, hr.locations.state_province
from  hr.employees
join hr.departments on hr.employees.department_id = hr.departments.department_id
join hr.locations on hr.locations.location_id = hr.departments.location_id
where hr.employees.first_name like '%z%'

-- 6. all departments including those where does not have any employee.
select * from hr.departments

-- 7. the first and last name and salary for those employees who earn less than the employee earn whose number is 182.
select hr.employees.first_name, hr.employees.last_name, hr.employees.salary
from  hr.employees
where hr.employees.salary < (select hr.employees.salary from hr.employees where hr.employees.employee_id = 182)

-- 8. the first name of all employees including the first name of their manager.
select em.first_name as employee
from hr.employees em
left join hr.employees mn on mn.employee_id = em.manager_id
where em.first_name  =  mn.first_name

-- 9. the department name, city, and state province for each department.
select hr.departments.department_name, hr.locations.city, hr.locations.state_province
from  hr.departments
join hr.locations on hr.locations.location_id = hr.departments.location_id

--10. the first name, last name, department number and name, for all employees who have or have not any department.
select hr.employees.first_name, hr.employees.last_name, hr.departments.department_id, hr.departments.department_name
from  hr.employees
left join hr.departments on hr.employees.department_id = hr.departments.department_id

--11. the first name of all employees and the first name of their manager including those who does not working under any manager.
select
    em.first_name as employee,
    mn.first_name as manager

from
    hr.employees em
left join hr.employees mn on mn.employee_id = em.manager_id

--12. the first name, last name, and department number for those employees who works in the same department as the employee who holds the last name as taylor.
select hr.employees.first_name, hr.employees.last_name, hr.departments.department_id
from  hr.employees
left join hr.departments on hr.employees.department_id = hr.departments.department_id
where hr.employees.department_id in (select hr.employees.department_id from hr.employees where hr.employees.last_name = 'taylor')

--13. the job title, department name, full name (first and last name ) of employee, and
--starting date for all the jobs which started on or after 1st january, 1993 and ending with on or before 31 august, 1997.
select hr.employees.first_name || ' ' || hr.employees.last_name as full_name , hr.departments.department_name, hr.jobs.job_title
from  hr.employees
left join hr.departments on hr.employees.department_id = hr.departments.department_id
left join hr.jobs on hr.employees.job_id = hr.jobs.job_id
join hr.job_history on hr.employees.employee_id = hr.employees.employee_id
where hr.job_history.start_date >= to_date('01-dec-93','dd-mon-yy')
and hr.job_history.end_date <= to_date('31-aug-97','dd-mon-yy')

--14. job title, full name (first and last name ) of employee, and the difference between maximum salary for the job and salary of the employee.
select hr.employees.first_name || ' ' || hr.employees.last_name as full_name, hr.jobs.job_title, hr.jobs.max_salary - hr.employees.salary as difference_with_max_salary
from  hr.employees
left join hr.jobs on hr.employees.job_id = hr.jobs.job_id
join hr.job_history on hr.employees.employee_id = hr.employees.employee_id

--15. the name of the department, average salary and number of employees working in that department who got commission.
select hr.departments.department_name, count(hr.employees.employee_id), avg(hr.employees.salary)
from hr.employees
left join hr.departments on hr.employees.department_id = hr.departments.department_id
where hr.employees.commission_pct is not null
group by hr.departments.department_name

--16. the full name (first and last name ) of employee, and job title of those employees who is working in the department which id is 80.
select hr.employees.first_name || ' ' || hr.employees.last_name as full_name, hr.jobs.job_title
from  hr.employees
left join hr.jobs on hr.employees.job_id = hr.jobs.job_id
where  hr.employees.department_id = 80

--17. the name of the country, city, and the departments which are running there.
select hr.countries.country_name, hr.locations.city, hr.departments.department_name
from hr.departments
join hr.locations on hr.locations.location_id = hr.departments.location_id
join hr.countries on hr.locations.country_id = hr.countries.country_id

--18. department name and the full name (first and last name) of the manager.
select hr.departments.department_name, hr.employees.first_name || ' ' || hr.employees.last_name as full_name
from  hr.employees
left join hr.departments on hr.departments.manager_id = hr.employees.employee_id

--19. job title and average salary of employees.
select hr.jobs.job_title, avg(hr.employees.salary)
from hr.employees
left join hr.jobs on hr.employees.job_id = hr.jobs.job_id
group by hr.jobs.job_title

--20. the details of jobs which was done by any of the employees who is presently earning a salary on and above 12000.
select hr.jobs.job_title
from hr.employees
join hr.jobs on hr.employees.job_id = hr.jobs.job_id
where hr.employees.salary >= 12000
group by hr.jobs.job_title

--21. the country name, city, and number of those departments where at leaste 2 employees are working.
select hr.employees.department_id, hr.countries.country_name, hr.locations.city
from  hr.departments
join hr.employees on hr.departments.manager_id = hr.employees.employee_id
join hr.locations on hr.locations.location_id = hr.departments.location_id
join hr.countries on hr.locations.country_id = hr.countries.country_id
where hr.employees.department_id in (
select hr.employees.department_id
from  hr.employees
group by hr.employees.department_id
having count(hr.employees.employee_id) > 2)

--22. the department name, full name (first and last name) of manager, and their city.
select hr.departments.department_name, hr.employees.first_name || ' ' || hr.employees.last_name as manager_full_name, hr.locations.city
from  hr.employees
join hr.departments on hr.departments.manager_id = hr.employees.employee_id
join hr.locations on hr.locations.location_id = hr.departments.location_id

--23. the employee id, job name, number of days worked in for all those jobs in department 80.
select hr.employees.employee_id, hr.jobs.job_title, hr.job_history.end_date - hr.job_history.start_date
from  hr.employees
left join hr.jobs on hr.employees.job_id = hr.jobs.job_id
left join hr.job_history on hr.employees.employee_id = hr.job_history.employee_id
where hr.employees.department_id = 80

--24. the full name (first and last name), and salary of those employees who working in any department located in london.
select hr.employees.first_name || ' ' || hr.employees.last_name as full_name, hr.employees.salary
from  hr.employees
join hr.departments on hr.departments.department_id = hr.employees.department_id
join hr.locations on hr.locations.location_id = hr.departments.location_id
where hr.locations.city = 'london'

--25. full name(first and last name), job title, starting and ending date of last jobs for those employees with worked without a commission percentage.
select hr.employees.first_name || ' ' || hr.employees.last_name as full_name, hr.jobs.job_title, hr.job_history.start_date, hr.job_history.end_date
from hr.employees
join hr.jobs on hr.jobs.job_id = hr.employees.job_id
join hr.job_history on hr.jobs.job_id = hr.job_history.job_id
where hr.employees.commission_pct is null

--26. the department name and number of employees in each of the department.
select hr.departments.department_name, count(hr.employees.employee_id)
from  hr.employees
left join hr.departments on hr.departments.department_id = hr.employees.department_id
group by hr.departments.department_name

--27. the full name (firt and last name ) of employee with id and name of the country presently where (s)he is working.
select hr.employees.first_name || ' ' || hr.employees.last_name as full_name, hr.locations.city
from  hr.employees
join hr.departments using(department_id)
join hr.locations using(location_id)
join hr.countries using (country_id)

--28. the name ( first name and last name ) for those employees who gets more salary than the employee whose id is 163.
select hr.employees.first_name || ' ' || hr.employees.last_name as full_name
from  hr.employees
where hr.employees.salary > (select salary from hr.employees where employee_id = 163)

--29. the name ( first name and last name ), salary, department id, job id for those employees who works in the same designation as the employee works whose id is 169.
select employees.first_name || ' ' || hr.employees.last_name as full_name,hr.employees.department_id,  hr.employees.salary, employees.job_id
from  hr.employees
where hr.employees.job_id = (select job_id from hr.employees where employee_id = 169)

--30. the name ( first name and last name ), salary, department id for those employees who earn such amount of salary which is the smallest salary of any of the departments.
select first_name || ' ' || last_name as full_name, department_id,  salary
from employees
where salary in (select min(salary)
                 from employees
                 group by department_id );

--31. the employee id, employee name (first name and last name ) for all employees who earn more than the average salary.
select employee_id, first_name || ' ' || last_name as full_name
from employees
where salary > (select avg(salary) from employees);

--32. the employee name ( first name and last name ), employee id and salary of all employees who report to payam.
select
    em.first_name || ' ' || em.last_name as full_name
from employees em
        inner join employees mn on mn.employee_id = em.manager_id
where  mn.first_name = 'payam'

--33. the department number, name ( first name and last name ), job and department name for all employees in the finance department 100.
select d.department_id, d.department_name, first_name, last_name, j.job_title, d.department_name
from employees
join departments d on employees.department_id = d.department_id
join jobs j on employees.job_id = j.job_id
where department_name = 'finance'

--34. all the information of an employee whose salary and reporting person id is 3000 and 121 respectively.
select  * from employees where salary = 3000 and manager_id = 121

--35. all the information of an employee whose id is any of the number 134, 159 and 183.   
select * from employees where employee_id in (134,159,183);

--36. all the information of the employees whose salary is within the range 1000 and 3000.
select * from employees where salary between 1000 and 3000;

--37. all the information of the employees whose salary is within the range of smallest salary and 2500.
select * from employees where salary between (select min(salary) from employees) and 3000;

--38. all the information of the employees who does not work in those departments where some employees works whose id within the range 100 and 200.
select * from employees where department_id not in ((select distinct department_id from employees where employee_id between 100 and 200));

--39. all the information for those employees whose id is any id who earn the second highest salary. hmmmm

--40. the employee name( first name and last name ) and hire_date for all employees in the same department as clara. exclude clara.
select first_name || ' ' || last_name as employee_name, hire_date from employees where department_id = (select department_id from employees where first_name = 'clara') and first_name <> 'clara';

--41. the employee number and name( first name and last name ) for all employees who work in a department with any employee whose name contains a t.
select employee_id, first_name || ' ' || last_name as employee_name from employees where department_id in (select distinct department_id from employees where first_name like '%t%');

--42. the employee number, name( first name and last name ), and salary for all employees who earn more than the average salary and who work in a department with any employee with a j in their name.
select employee_id, first_name || ' ' || last_name as employee_name, salary from employees
where salary > (select avg(salary) from employees) and department_id in (select distinct department_id from employees where first_name like '%j%');

--43. the employee name( first name and last name ), employee id, and job title for all employees whose department location is toronto.
select first_name || ' ' || last_name as employee_name, employee_id, job_title
from employees
join jobs j on employees.job_id = j.job_id
join departments d on employees.department_id = d.department_id
join locations on d.location_id = locations.location_id
where city = 'toronto';

--44. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is mk_man.
select employee_id, first_name || ' ' || last_name as employee_name, j.job_title
from employees
join jobs j on employees.job_id = j.job_id
where salary < (select min(salary) from employees where job_id = 'mk_man');

--45. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is mk_man. exclude job title mk_man.
select employee_id, first_name || ' ' || last_name as employee_name, j.job_title
from employees
join jobs j on employees.job_id = j.job_id
where salary < (select min(salary) from employees where job_id = 'mk_man') and j.job_id <> 'mk_man';

--46. all the information of those employees who did not have any job in the past.
select * from employees where employee_id not in (select employee_id from job_history);

--47. the employee number, name( first name and last name ) and job title for all employees whose salary is more than any average salary of any department.
select employee_id, first_name || ' ' || last_name as employee_name, job_title
from employees
join jobs using (job_id)
where salary > all ( select avg(salary) from employees group by department_id );

--48. the employee name( first name and last name ) and department for all employees for any existence of those employees whose salary is more than 3700.
select  first_name || ' ' || last_name as employee_name, department_id from employees where salary > 3700;

--49. the department id and the total salary for those departments which contains at least one salaried employee.
select department_id, sum(salary)
from employees
where department_id is not null
group by department_id;

--50. the employee id, name ( first name and last name ) and the job id column with a modified title salesman for those employees whose job title is st_man and developer for whose job title is it_prog.
select  employee_id,  first_name || ' ' || last_name as employee_name,
        case job_id
            when 'st_man' then 'salesman'
            when 'it_prog' then 'developer'
            else job_id
            end as job,  salary
from employees;

--51. the employee id, name ( first name and last name ), salary and the salarystatus column with a title high and low respectively for those employees whose salary is more than and less than the average salary of all employees.
select  employee_id,  first_name, last_name, salary,
        case when salary >= (select avg(salary) from employees) then 'high' else 'low' end as salarystatus
from employees;

--52. the employee id, name ( first name and last name ), salarydrawn, avgcompare (salary - the average salary of all employees) and the salarystatus column with a title high and low respectively for those employees whose salary is more than and less than the average salary of all employees.
select  employee_id,  first_name, last_name,  salary as salarydrawn,
round((salary -(select avg(salary) from employees)),2) as avgcompare,
case  when salary >= (select avg(salary) from employees) then 'high' else 'low' end as salarystatus
from employees;

--53. a set of rows to find all departments that do actually have one or more employees assigned to them.
select  * from departments where department_id in (select distinct(department_id) from employees);

--54. all employees who work in departments located in the united kingdom.
select employees.*
from employees
         join departments d on employees.department_id = d.department_id
         join locations on d.location_id = locations.location_id
join countries c on locations.country_id = c.country_id
where country_name = 'united kingdom';

--55. all the employees who earn more than the average and who work in any of the it departments.
select employees.*
from employees
join departments d on employees.department_id = d.department_id
where salary > (select avg(salary) from employees) and department_name = 'it';

--56. who earns more than mr. ozer.
select * from employees where salary > (select salary from employees where last_name = 'ozer');

--57. which employees have a manager who works for a department based in the us.
select first_name,last_name from employees
where manager_id in
      (select employee_id from employees
       where department_id in
             (select department_id from departments
              where location_id in
                    (select location_id from locations
                     where country_id='us')));

--58. the names of all employees whose salary is greater than 50% of their department’s total salary bill.
select first_name
from employees e1
where salary > ( select (sum(salary))*0.5 from employees e2 where e1.department_id=e2.department_id);

--59. the details of employees who are managers.
select * from employees where employee_id in (select manager_id from employees);

--60. the details of employees who manage a department.
select * from employees where employee_id in (select manager_id from departments);

--61. the employee id, name ( first name and last name ), salary, department name and city for all the employees who gets the salary as the salary earn by the employee which is maximum within the joining person january 1st, 2002 and december 31st, 2003.
select employee_id, first_name || ' ' || last_name as employee_name, salary, department_name, city from employees
    join departments d on employees.department_id = d.department_id
join locations l2 on d.location_id = l2.location_id
where salary = (select max(salary) from employees where hire_date between ('02-jan-01') and ('03-dec-31'))

--62. the department code and name for all departments which located in the city london.
select department_id, department_name from departments join locations l on departments.location_id = l.location_id where city = 'london'

--63. the first and last name, salary, and department id for all those employees who earn more than the average salary and arrange the list in descending order on salary.
select first_name, last_name , salary, department_id
from employees where salary > (select avg(salary) from employees )
order by salary desc;

--64. the first and last name, salary, and department id for those employees who earn more than the maximum salary of a department which id is 40.
select first_name, last_name, salary, department_id
from employees
where salary > (select max(salary) from employees where department_id = 40);

--65. the department name and id for all departments where they located, that id is equal to the id for the location where department number 30 is located.
select department_name, department_id
from departments
where location_id = ( select location_id from departments where department_id = 30);

--66. the first and last name, salary, and department id for all those employees who work in that department where the employee works who hold the id 201.
select first_name, last_name, salary, department_id
from employees
where department_id = (select department_id from employees where employee_id = 201);

--67. the first and last name, salary, and department id for those employees whose salary is equal to the salary of the employee who works in that department which id is 40.
select first_name, last_name, salary, department_id
from employees where salary in (select salary from employees where department_id = 40);

--68. the first and last name, and department code for all employees who work in the department marketing.
select first_name, last_name, department_id
from employees where department_id = (select department_id from departments where department_name = 'marketing');

---69. the first and last name, salary, and department id for those employees who earn more than the minimum salary of a department which id is 40.
select  first_name, last_name, salary, department_id from employees where salary > (select min(salary) from employees where department_id = 40)

--70. the full name,email, and designation for all those employees who was hired after the employee whose id is 165.
select  first_name, last_name, email, job_id from employees where hire_date > (select hire_date from employees where employee_id = 165)

--71. the first and last name, salary, and department id for those employees who earn less than the minimum salary of a department which id is 70.
select  first_name, last_name, salary, department_id from employees where salary < (select min(salary) from employees where department_id = 70)

--72. the first and last name, salary, and department id for those employees who earn less than the average salary, and also work at the department where the employee laura is working as a first name holder.
select first_name, last_name, salary, department_id
 from employees
   where salary <
      (select avg(salary)
        from employees )
         and department_id =
               (select department_id
                 from employees
                  where first_name = 'laura');

--73. the city of the employee whose id 134 and works there.
select city
	from locations
		where location_id =
    (select location_id from departments
			where department_id =
             	(select department_id from employees
						where employee_id=134));

--74. the the details of those departments which max salary is 7000 or above for those employees who already done one or more jobs.
select * from departments
where department_id in
    (select department_id from employees where employee_id in
         (select employee_id from job_history group by employee_id having count(employee_id) > 1)
     group by department_id
     having max(salary) > 7000);

--75. the detail information of those departments which starting salary is at least 8000.
select * from departments where department_id in ( select department_id from employees group by department_id having min(salary)>=8000);

--76. the full name (first and last name) of manager who is supervising 4 or more employees.
select first_name || ' ' || last_name as manager from employees where employee_id in (select manager_id from employees group by manager_id having count(*)>=4);

--77. the details of the current job for those employees who worked as a sales representative in the past.
select employees.employee_id, jobs.job_id, job_title
from employees
join jobs on employees.job_id = jobs.job_id
where employee_id = (select employee_id
                     from job_history
                     join jobs on job_history.job_id = jobs.job_id
                     where job_title = 'sales representative')

--78. all the infromation about those employees who earn second lowest salary of all the employees.
select * from employees e where  2 = (select count(distinct salary ) from employees where salary <= e.salary);

--79. the details of departments managed by susan.
select * from departments where manager_id in (select employee_id from employees where first_name='susan');

--80. the department id, full name (first and last name), salary for those employees who is highest salary drawar in a department.
select department_id, first_name || ' ' || last_name as full_name, salary
from employees e where salary = (select max(salary) from employees where department_id = e.department_id);
