-- --------------------------------------------------------
-- --  Trigger ON DEPARTMENTS AFTER DELETE OR INSERT
-- --------------------------------------------------------
create or replace trigger UPDATE_DEPARTMENTS_AMOUNT
    after insert or delete
    on DEPARTMENTS
    for each row
declare
    number_of_departments NUMBER(8);
BEGIN

    IF DELETING THEN
        select DEPARTMENT_AMOUNT into number_of_departments from LOCATIONS where LOCATION_ID = :OLD.LOCATION_ID;

        number_of_departments := (number_of_departments - 1);

        update LOCATIONS
        set LOCATIONS.DEPARTMENT_AMOUNT = number_of_departments
        where LOCATION_ID = :OLD.LOCATION_ID;
    END IF;

    IF INSERTING THEN
        select DEPARTMENT_AMOUNT into number_of_departments from LOCATIONS where LOCATION_ID = :NEW.LOCATION_ID;

        number_of_departments := (number_of_departments + 1);

        update LOCATIONS
        set LOCATIONS.DEPARTMENT_AMOUNT = number_of_departments
        where LOCATION_ID = :NEW.LOCATION_ID;
    END IF;
END;
/