--------------------------------------------------------
--  Add columns for Table EMPLOYEES
--------------------------------------------------------

ALTER TABLE EMPLOYEES
    ADD (CARD_NUMBER NUMBER(16));

--------------------------------------------------------
--  Ref Constraints for Table EMPLOYEES
--------------------------------------------------------

ALTER TABLE EMPLOYEES
    ADD CONSTRAINT EMP_CARD_FK UNIQUE FOREIGN KEY (CARD_NUMBER)
        REFERENCES PAY (CARD_NUMBER);

--------------------------------------------------------
--  Ref Constraints for Table EMPLOYEE_SPENT_TIME
--------------------------------------------------------

ALTER TABLE employee_spent_time
    ADD CONSTRAINT employee_spent_time_project_FK FOREIGN KEY (project_id)
        REFERENCES projects (project_id);

ALTER TABLE employee_spent_time
    ADD CONSTRAINT employee_spent_time_employee_FK FOREIGN KEY (employee_id)
        REFERENCES employees (employee_id);