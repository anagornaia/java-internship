CREATE TABLE projects
(
    project_id              NUMBER(6) PRIMARY KEY,
    project_description     VARCHAR(1500) NOT NULL,
    project_investments     NUMBER(8, -3) NOT NULL,
    project_revenue         NUMBER(8, 2),
    CONSTRAINT PROJECTS_PR_DESC_CK CHECK ( LENGTHB(project_description) > 10 ),
    CONSTRAINT PROJECTS_PR_INVEST_CK CHECK (LENGTHB(project_investments) > 0 )
);