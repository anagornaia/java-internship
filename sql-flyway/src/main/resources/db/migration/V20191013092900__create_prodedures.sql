-- --------------------------------------------------------
-- -- ADD LOG
-- --------------------------------------------------------
CREATE OR REPLACE PROCEDURE employment_history_log
(empployee_first_name VARCHAR, employee_last_name VARCHAR, empl_action VARCHAR) AS
BEGIN
    insert into EMPLOYMENT_LOGS (first_name, last_name, employment_action, employment_status_updtd_tmstmp)
    values (empployee_first_name,employee_last_name, empl_action, CURRENT_TIMESTAMP);
END;
/