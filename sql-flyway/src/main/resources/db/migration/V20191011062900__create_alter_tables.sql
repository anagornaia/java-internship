--------------------------------------------------------
--  Add Column for Table LOCATIONS
--------------------------------------------------------
ALTER TABLE LOCATIONS
   ADD (department_amount  NUMBER(4) DEFAULT 0);

COMMENT ON COLUMN LOCATIONS.department_amount IS 'Contains the amount of departments in the location';
