--------------------------------------------------------
--  Insert test data into db
--------------------------------------------------------
insert into HOMEWORK.REGIONS (REGION_ID, REGION_NAME) VALUES (1, 'Asia');

insert into HOMEWORK.COUNTRIES (COUNTRY_ID, COUNTRY_NAME, REGION_ID) VALUES ('JP', 'Japan', 1);

insert into HOMEWORK.LOCATIONS (LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, STATE_PROVINCE, COUNTRY_ID)
    VALUES (1, 'Street', 1234, 'City', 'State', 'JP');

insert into HOMEWORK.DEPARTMENTS (DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID) values (1, 'IT', null, 1);

insert into HOMEWORK.JOBS (JOB_ID, JOB_TITLE) VALUES ('IT_PROG',	'Programmer');

insert into HOMEWORK.EMPLOYEES (EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID)
VALUES(1, 'John', 'Doe', 'e@mail.com', '123-456-1', current_timestamp, 'IT_PROG', 10000, 0, null, 1);

insert into HOMEWORK.JOB_HISTORY(EMPLOYEE_ID, START_DATE, END_DATE, JOB_ID, DEPARTMENT_ID) values (1, '19-JAN-01', '19-JAN-20' , 'IT_PROG', 1);