CREATE TABLE employee_spent_time
(
    employee_id             NUMBER(6),
    project_id              NUMBER(6),
    spent_time              NUMBER(6) NOT NULL,
    PRIMARY KEY(employee_id, project_id)
);