CREATE TABLE employment_logs
(
    employment_log_id NUMBER(4) PRIMARY KEY,
    first_name VARCHAR2(20),
    last_name VARCHAR2(25),
    employment_action VARCHAR2(5) CHECK( employment_action IN ('HIRED','FIRED') ),
    employment_status_updtd_tmstmp TIMESTAMP
);

CREATE SEQUENCE employment_logs_seq
START WITH 1
INCREMENT BY 1
MAXVALUE 9999;

ALTER TABLE employment_logs
MODIFY employment_log_id NUMBER(4) DEFAULT employment_logs_seq.nextval;
