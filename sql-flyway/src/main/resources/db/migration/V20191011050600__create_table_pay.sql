CREATE TABLE pay
(
    card_number      NUMBER(16) PRIMARY KEY,
    salary           NUMBER(8, 2),
    commission_pct   NUMBER(2, 2)
);