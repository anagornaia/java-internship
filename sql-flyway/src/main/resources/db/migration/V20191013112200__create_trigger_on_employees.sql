-- --------------------------------------------------------
-- --  Trigger ON EMPLOYEES AFTER DELETE OR INSERT
-- --------------------------------------------------------

create or replace trigger log_employee
    after insert or delete
    on EMPLOYEES
    for each row
BEGIN
    IF DELETING THEN
        employment_history_log(:OLD.FIRST_NAME, :OLD.LAST_NAME, 'FIRED');
    END IF;

    IF INSERTING THEN
        employment_history_log(:NEW.FIRST_NAME, :NEW.LAST_NAME, 'HIRED');
    END IF;
END;
/