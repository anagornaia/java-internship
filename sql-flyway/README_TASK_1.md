#PART I: CREATING AND MODIFYING TABLES
1.	Create a maven project and use flyway to migrate attached scripts (hr_scripts.7z) into your local DB
2.	Insert one row into job_history table
3.	Create table pay with a one-to-one relation to employees
* cardNr(PK)
* salary(transfered from employees)
* commission_pct(transfered from employees)
4.	Create table projects with a many-to-many relation to employees
* project_id (PK)
* project_description (constaint: the text length should be greater than 10)
* project_investments 
    * constraint: value should be greater than 0
    * investments should be measured in thousands (the number should end with at least 3 zeros: 
    * anything less than 500 is 0. 
    * anything between 500 and 1499 should be 1000, 
    * anything between 1500 and 2499 should be 2000  
    * (hint: investigate number data type precision and scale)
* project_revenue
* somewhere should be stored information about amount of hours an employee has been working on a project
